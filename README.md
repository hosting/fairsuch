# k8s searxng 

## serxng with kustomize 

templated from HELM chart https://github.com/searxng/searxng-helm-chart/tree/main/searxng

## settings

create your `settings.yml` changes as base64 secret und put it in `secret.yaml`

with `use_default_settings` it will always use a fresh engine list from the image and patch your `settings.ym`l to `/etc/searxng.settings.yml` in your pod

## deploy

```
kustomize build dev > searxngbuilddev.yaml     
less searxngbuilddev.yaml     
kubectl -n fairsuch-dev apply -f searxngbuilddev.yaml 
```

## credits

searxng community

kustomize chart: roland.alton.at

## license

AGPL
