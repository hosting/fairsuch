## Über fairsuch 

fairsuch ist eine [Metasuchmaschine](https://de.wikipedia.org/wiki/Metasuchmaschine), welche die Ergebnisse von über 100 [Suchmaschinen](https://fairsuch.net/preferences) sammelt und aufbereitet, ohne dabei Informationen über einen selbst zu sammeln oder an andere Suchmaschinen weiterzugeben.

fairsuch basiert auf dem SearXNG Projekt und wird gemeinschaftlich entwickelt. Ziel ist es, wieder Souveränität im Internet zu erlangen und keine Daten an Konzerne weiterzugeben.

Die Technologie von fairsuch kann auch in einer Organisation integriert werden, um auch etwa Dokumente aus dem Intranet durchsuchen und anzeigen zu können. 

Mehr Informationen sind in der [SearXNG Dokumentation](https://docs.searxng.org/) zu finden.


### Welche Vorteile kann ich von fairsuch erwarten?

- fairsuch bietet keine personalisierten Ergebnisse wie bspw. Google, aber es erstellt auch kein Profil über eine Person
- fairsuch gibt niemals etwas an Dritte weiter
- Die Basis SearXNG ist freie Software, der Code ist zu 100% offen und jede:r ist willkommen diesen zu verbessern
- endloses Scrollen ohne Seiten blättern
- Hervorhebung eines Wikipedia Artikels - wenn verfügbar
- erweiterte Einstellung mit Einschränkung des Zeitraumes (Tag/ Woche/ Monat/ Jahr)
- Einstellungsmöglichkeit für sichere Suche ohne anstössige oder schlecht bewertete Inhalte


### Was passiert mit meinen Daten?

Alle Suchanfragen werden verschlüsselt und sind komplett anonym. Der Schutz der Privatsphäre bei der Verarbeitung personenbezogener Daten ist uns ein wichtiges Anliegen. [Wir verarbeiten Daten](https://www.fairkom.eu/privacy) auf Grundlage der geltenden Fassung der [Datenschutzgrundverordnung in Österreich](https://www.dsb.gv.at/recht-entscheidungen/gesetze-in-oesterreich.html). 

Diese Informationen werden in Cookies auf deinem Rechner gespeichert, damit wir auf dem Server keine persönlichen Daten speichern müssen.
Diese Cookies sind technisch für die Suche notwendig und dienen einzig dem Komfort. Wir verwenden sie nicht, um zu überwachen oder ein Profil zu erstellen.

Die Server der fairkom Gesellschaft stehen in den Rechenzentren von  Hetzner in Nürnberg und Helsinki sowie Internex in Wien und Gmünd. Mit beiden wurden Auftragsverarbeitungsvereinbarungen abgeschlossen. Hetzner betreibt seine Standorte CO2 neutral.


### Erhalte ich personalisierte Werbung oder Vorschläge aufgrund meiner Aktivitäten?

Nutzer:innen werden weder verfolgt noch werden Daten gesammelt. Von Suchmaschinen angebotene Tracking-Cookies werden blockiert, somit werden keine personalisierten Ergebnisse, Werbeanzeigen oder Vorschläge angezeigt.

Die Gewichtung der verwendeten Suchmaschinen ist meist gleich, einige Quellen werden etwas weiter oben angezeigt, siehe dazu die [https://fairsuch.net/preferences](Einstellungen), wo du auch einzelne Suchmaschinen an- und ausschalten kannst, die befragt werden.


### Wie kann ich fairsuch als Standardsuchmaschine festlegen?

fairsuch unterstützt [OpenSearch](https://github.com/dewitt/opensearch/blob/master/opensearch-1-1-draft-6.md). Wir haben eine [Anleitung](https://git.fairkom.net/hosting/fairsuch/-/wikis/fairsuch-einrichten) zusammengestellt. Weitere Informationen zum Ändern der Standardsuchmaschine finden Sie in der Dokumentation zu Ihrem [WEB-Browser](https://de.wikipedia.org/wiki/Webbrowser):
- [Firefox](https://support.mozilla.org/en-US/kb/add-or-remove-search-engine-firefox)
- [Microsoft Edge](https://support.microsoft.com/en-us/help/4028574/microsoft-edge-change-the-default-search-engine) – Hier finden sich auch nützliche Hinweise zu Chrome und Safari.
- [Chromium](https://www.chromium.org/tab-to-search)-basierte Browser fügen nur Websites hinzu, zu denen Benutzende ohne Pfadangabe navigieren.

Der Code für eine Suchanfrage lautet:  https://fairsuch.net/search?q=

### Wie kann ich fairsuch auf meinem mobilen Gerät verwenden?

Über das Aufrufen der Seite [fairsuch.net](https://fairsuch.net/) können Suchanfragen in das Feld eingegeben werden. Bei den Einstellungen der meisten Browser gibt es einen Menüpubkt "Suchmaschinen", wo auch dann die  kürzlich besuchte Suchmaschine fairsuch als Standardsuchmaschine aktiviert werden kann. 


### Was bedeutet "Im Cache" bei einem Suchergebnis?

Steht bei einem Suchergebnis "Im Cache" zeigt dies an, dass die Seite in einem Archiv verfügbar ist. Ein Aufruf ist demnach auch möglich, ohne die Seite direkt besuchen zu müssen, etwa wenn ein Tracking nicht gewollt ist.


### Welche Einstellungen kann ich bei fairsuch vornehmen?

In den [Einstellungen](https://fairsuch.net/preferences) können neben Sprache und Standardkategorien, Änderungen der Benutzeroberfläche und der Privatsphäre vorgenommen werden. Auch die Verwaltung der verwendeten Suchmaschinen ist hier möglich.

Die [Suchmaschinenstatistik](https://fairsuch.net/stats) enthält einige nützliche Statistiken über die verwendeten Suchmaschinen.

### Wo kann ich Fragen oder Verbesserungsvorschläge einbringen?

Als gitlab [Issues](https://git.fairkom.net/hosting/fairsuch/-/issues) können Rückmeldungen  geschildert werden. Wir bitten um Verständnis, wenn nicht alle Wünsche umsetzbar sind. Vorher bitte bei [searxng auf github](https://github.com/searxng/searxng/issues) nachsehen, ob das Problem schon mal berichtet worden ist.


### Wie finanziert sich fairsuch?
Betreiber ist die [fairkom Gesellschaft](https://www.fairkom.eu/about) mit Sitz in Dornbirn, Österreich. Diese hat es sich unter anderem zur Aufgabe gemacht, Alternativen zu GAFAM (Google, Apple, Facebook, Amazon, Microsoft) bereitzustellen. Neben fairsuch werden zahlreiche Alternativen auf [fairapps.net](https://fairapps.net/) angeboten wie [fairmeeting](https://www.fairkom.eu/fairmeeting), [faircloud](https://www.fairkom.eu/faircloud), [fair.tube](https://www.fairkom.eu/fairtube) oder [fairchat](https://www.fairkom.eu/fairchat). Wer mehr Features oder Speicherplatz wünscht oder die Dienste regelmässig oder beruflich verwendet soll ein [Paket bestellen](https://shop.fairkom.net/paketevergleich/#fairapps-pakete-im-vergleich). [fairkom](https://www.fairkom.eu/) [berät und begleitet auch Unternehmen und Organisationen beim Umstieg](https://www.fairkom.eu/consulting) und finanziert so Infrastruktur und Personal. Wenn du einfach nur etwas beitragen möchtest, dass unsere Dienste offenbleiben, dann [spende](https://www.paypal.com/donate/?hosted_button_id=S2F69CNSVHLN2) doch was. 


### Wie kann ich einen eigenen SearXNG Server betreiben?

Wer mit dem Betrieb von WEB-Servern vertraut ist, kann auch eine eigene Metasuch-Instanz einrichten; die Software dazu kann über die [SearXNG Quellen](https://github.com/searxng/searxng) bezogen werden. Weitere Informationen zur Installation und zum Betrieb finden sich in der [SearXNG Dokumentation](https://github.com/searxng/searxng).
Fügen Sie Ihre Instanz zu der Liste der öffentlich zugänglichen Instanzen hinzu um auch anderen Menschen zu helfen ihre Privatsphäre zurückzugewinnen und das Internet freier zu machen. Je dezentraler das Internet ist, desto mehr Freiheit haben wir!

[SearXNG Quellen]: {{GIT_URL}}
[#searxng:matrix.org]: https://matrix.to/#/#searxng:matrix.org
[SearXNG Dokumentation]: {{get_setting('brand.docs_url')}}
[searx]: https://github.com/searx/searx
[Metasuchmaschine]: https://de.wikipedia.org/wiki/Metasuchmaschine
[Weblate]: https://weblate.bubu1.eu/projects/searxng/
[Seeks-Projekt]: https://beniz.github.io/seeks/
[OpenSearch]: https://github.com/dewitt/opensearch/blob/master/opensearch-1-1-draft-6.md
[Firefox]: https://support.mozilla.org/en-US/kb/add-or-remove-search-engine-firefox
[Microsoft Edge]: https://support.microsoft.com/en-us/help/4028574/microsoft-edge-change-the-default-search-engine
[Chromium]: https://www.chromium.org/tab-to-search
[WEB-Browser]: https://de.wikipedia.org/wiki/Webbrowser
