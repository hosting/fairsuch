## About fairsuch

fairsuch is a [meta search engine](https://en.wikipedia.org/wiki/Metasearch_engine) which collects and processes the results of more than 100 [search engines](https://fairsuch.net/preferences) without collecting information about oneself or passing on to other search engines. "suchen" is german for "search", that's why we called it fairsuch, results could be fair such.

fairsuch is based on the SearXNG project and is developed jointly. The aim is to regain sovereignty on the Internet and not to disclose your personal preferences to tech giants.

The technology of fairsuch can also be integrated in an organization to highlight content of its own homepage or to browse and view documents from its Intranet.

More information can be found in the [SearXNG Documentation](https://docs.searxng.org/).


### What advantages can I expect from fairsuch?

- fairsuch does not offer personalized results such like e.g. Google does
- fairsuch does not create a profile about a person
- the software base SearXNG is free software, the code is open and anybody is welcome to investigate or improve it
- endless scrolling without pagination
- highlighting result as an Wikipedia article - if available
- extended setting with limitation of the period
- possibility to set a secure search without any offensive or badly rated content

### Special search

- Prepend the search engine with ! so it will only be searched with this one, e.g. !goo only searches with Google - abbreviations see settings / search engines
- Translation (shown in Infobox): !mozhi en-de Hello World translate Hello World from English to German.
- IP shows your IP address

### What happens to my data?

All searches are encrypted and are completely anonymous. Protection of privacy in the processing of personal data is an important concern to us. [We process data](https://www.fairkom.eu/privacy) based on the current version of the [Privacy General Data Protection Regulation in Austria](https://www.dsb.gv.at/recht-decisionen/gesetze-in-oesterreich.html).

You can set preferences (with which search engines you want to search), these are stored in cookies on your computer so that we do not have to store personal data on the server.
These cookies are technically necessary for the search for your comfort. We do not use them to monitor or create a profile.

The servers of fairkom are located in the data centers of Hetzner in Nuremberg and Helsinki as well as Internex in Vienna and Gmünd. With both, contract processing agreements were concluded. Hetzner runs its CO2 sites neutral.


### Do I receive personalized advertising or suggestions based on my activities?

We neither track nor collect data. Tracking cookies offered by search engines are blocked, so no personalized results, advertisements or suggestions are displayed.

The weighting of the search engines used is usually the same, some sources are displayed a little further above, see the [https://fairsuch.net/preferences](Settings), where you can also turn on and off individual search engines that are queried.


### How can I set fair search as a standard search engine?

fairsuch supports [OpenSearch](https://github.com/dewitt/opensearch/blob/master/opensearch-1-1-draft-6.md). We have compiled a [how-to](https://git.fairkom.net/hosting/fairsuch/-/wikis/fairsuch-installation). For more information on how to change the standard search engine, see the documentation on your [web browser](https://de.wikipedia.org/wiki/Webbrowser):
- [Firefox](https://support.mozilla.org/en-US/kb/add-or-remove-search-engine-firefox)
- [Microsoft Edge](https://support.microsoft.com/en-us/help/4028574/microsoft-edge-change-the-default-search-engine) – Here you can find useful hints about Chrome and Safari.
- [Chromium](https://www.chromium.org/tab-to-search)-based browsers add only websites to which users navigate without path indication.

The search request code is: https://fairsuch.net/search?q=

### How can I use fairsuch on my mobile device?

Using the page [fairsuch.net](https://fairsuch.net/) you can enter searches into the field. In the settings of most browsers there is a menu "Searching Machines" where the recently visited search engine can be activated as a standard search engine.


### What does "In Cache" mean in a search result?

If a search result "In Cache" indicates that the page is available in an archive. A call is therefore also possible without having to visit the page directly, for example if a tracking is not desired.


### What settings can I make when looking for fair?

The [Settings](https://fairsuch.net/preferences) can be used in addition to language and standard categories, changes in user interface and privacy. It is also possible to manage the search engines used here.

The [search engine statistics](https://fairsuch.net/stats) contains some useful statistics on the search engines used. However, we sometimes restart the deployment, and then statistics start again from zero.

### Where can I ask questions or suggestions for improvement?

Feedback can be described as gitlab [Issues](https://git.fairkom.net/hosting/fairsuch/issues). We ask for understanding if not all wishes are feasible. Before [searxng on github](https://github.com/searxng/searxng/issues) see if the problem has already been reported.


### How does fairseeking finance?
The operator is the [fairkom Gesellschaft](https://www.fairkom.eu/about) based in Dornbirn, Austria. Among other services, this has led to the task of providing alternatives to GAFAM (Google, Apple, Facebook, Amazon, Microsoft). In addition to fairchat, numerous alternatives are offered to [fairapps.net](https://fairapps.net/) such as [fairmeeting](https://www.fairkom.eu/fairmeeting), [faircloud](https://www.fairkom.eu/faircloud), [fair.tube](https://www.fairkom.eu/fair If you want more features or storage space, or the services are to be used regularly or professionally, a [order package](https://shop.fairkom.net/paketevergleich/#fairapps-pakete-im-vergleich). [fairkom](https://www.fairkom.eu/) If you just want to contribute something to keep our services open, then simply [pay what you want](https://www.paypal.com/donate/?hosted_button_id=S2F69CNSVHLN2).


### How can I run your own SearXNG server?

Those who are familiar with the operation of Docker or kubernetes can also set up their own metasearch instance; the software can be obtained via [SearXNG Sources](https://github.com/searxng/searxng). Further information on installation and operation can be found in [SearXNG Documentation](https://github.com/searxng/searxng).
Your own instance can then be added to the [list of public
instances]({{get_setting('brand.public_instances')}}) to help other people recover their privacy and make the Internet more free. The more decentralized the Internet, the more freedom we have!

[SearXNG sources]: {{GIT_URL}}
[#searxng:matrix.org]: https://matrix.to/#/#searxng:matrix.org
[SearXNG documentation]: {{get_setting('brand.docs_url')}}
[searx]: https://github.com/searx
[Metasuchmaschine]: https://de.wikipedia.org/wiki/Metasuchmaschine
[Weblate]: https://weblate.bu1.eu/projects/searxng/
[Seeks project]: https://beniz.github.io/seeks/
[OpenSearch]: https://github.com/dewitt/opensearch/blob/master/opensearch-1-1-draft-6.md
[Firefox]: https://support.mozilla.org/en-US/kb/add-or-remove-search-engine-firefox
[Microsoft Edge]: https://support.microsoft.com/en-us/help/4028574/microsoft-edge-change-the-default-search engine
[Chromium]: https://www.chromium.org/tab-to-search
[WEB-Browser]: https://de.wikipedia.org/wiki/Webbrowser
